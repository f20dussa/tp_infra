# UE Infrastructures d'Intégration
TP 2021
Florian Dussable - Nicolas Kirchhoffer - Léo Beaujean - Romain Orvoën

## Project structure

The different components of the project located under src are as follows:  
    - publishers  
    - subscribers  
    - servers  
    - front  

## How to build

### Prerequisites

- [ ] Golang compiler
- [ ] Node.js (package manager & compiler)
- [ ] Bash

Run build.sh located under src to build the whole project

## How to run

### Prerequisites

- [ ] Mosquitto broker
- [ ] MongoDB engine

Run your Mosquitto broker before running the publishers and the subscribers  
Run your MongoDB engine before running the database subscriber or the api server