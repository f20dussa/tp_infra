package random

import "math/rand"
import "time"

func Gen_rand_int(min int, max int) int {
	rand_seed := rand.NewSource(time.Now().UnixNano())
	rand_ptr := rand.New(rand_seed)
	rand_number := rand_ptr.Intn(max-min) + min
	return rand_number
}

func Gen_rand_int_around_value(min int, max int, variation int, current_value int) int {
	var offset = Gen_rand_int(-variation, variation)
	for current_value+offset > max || current_value+offset < min {
		offset = Gen_rand_int(-variation, variation)
	}
	return current_value + offset
}
