package sensor

import "os"
import "path/filepath"
import "fmt"
import "strconv"

func Parse_args()(int, string){
	var sensor_id int
	var sensor_airport string
	if(len(os.Args) != 3){
		fmt.Println("Usage: " + filepath.Base(os.Args[0]) + " <sensor_id> <sensor_airport>")
		fmt.Println("Example: " + filepath.Base(os.Args[0]) + " 1 NTE")
		os.Exit(1)
	}
	sensor_id, _ = strconv.Atoi(os.Args[1])
	sensor_airport = os.Args[2]
	return sensor_id, sensor_airport
}

func Gen_msg(sensor_id int, sensor_airport string, sensor_nature string, sensor_value string, timestamp string) string {
	var msg string = fmt.Sprintf("SENSOR %d - %s - %s - %s - %s", sensor_id, sensor_airport, sensor_nature, sensor_value, timestamp)
	return msg
}