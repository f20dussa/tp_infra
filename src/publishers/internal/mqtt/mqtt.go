package mqtt

import "fmt"
import mqtt "github.com/eclipse/paho.mqtt.golang"

var host string = "127.0.0.1"
var port int = 1883
var client mqtt.Client

func Connect(client_id string){
    opts := mqtt.NewClientOptions()
    opts.AddBroker(fmt.Sprintf("tcp://%s:%d", host, port))
    opts.SetClientID(client_id)
    client = mqtt.NewClient(opts)
	client.Connect()
	token := client.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
}

func Publish(topic string, msg string){
	fmt.Println("Publishing message : " + msg)
	client.Publish(topic, 1, false, msg)
}