package main

import "time"
import "strconv"
import sensor "publishers/internal/sensor"
import random "publishers/internal/random"
import mqtt "publishers/internal/mqtt"

var sensor_id int
var sensor_airport string
var sensor_nature string = "PRES"
var sensor_value_min int = 12
var sensor_value_max int = 16

func main(){
	sensor_id, sensor_airport = sensor.Parse_args()
	mqtt.Connect("pub_" + sensor_airport + "_" + sensor_nature + "_" + strconv.Itoa(sensor_id))
	for{
		var sensor_value string = strconv.Itoa(random.Gen_rand_int(sensor_value_min, sensor_value_max))
		var timestamp string = time.Now().Format(time.RFC3339)
		var msg string = sensor.Gen_msg(sensor_id, sensor_airport, sensor_nature, sensor_value, timestamp)
		mqtt.Publish(sensor_airport, msg)
		time.Sleep(time.Second * 2)
	}
}