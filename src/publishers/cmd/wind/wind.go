package main

import "time"
import "strconv"
import sensor "publishers/internal/sensor"
import random "publishers/internal/random"
import mqtt "publishers/internal/mqtt"

var sensor_id int
var sensor_airport string
var sensor_nature string = "WIND"
var sensor_value_min int = 0
var sensor_value_max int = 150

func main(){
	sensor_id, sensor_airport = sensor.Parse_args()
	mqtt.Connect("pub_" + sensor_airport + "_" + sensor_nature + "_" + strconv.Itoa(sensor_id))
	var sensor_value int = random.Gen_rand_int(sensor_value_min, sensor_value_max)
	for{
		sensor_value = random.Gen_rand_int_around_value(sensor_value_min, sensor_value_max, 5, sensor_value)
		var sensor_value_string string = strconv.Itoa(sensor_value)
		var timestamp string = time.Now().Format(time.RFC3339)
		var msg string = sensor.Gen_msg(sensor_id, sensor_airport, sensor_nature, sensor_value_string, timestamp)
		mqtt.Publish(sensor_airport, msg)
		time.Sleep(time.Second * 2)
	}
}
