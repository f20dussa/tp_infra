package main

import "os"
import "path/filepath"
import "fmt"
import "time"
import mqtt "subscribers/internal/mqtt"
import handlers "subscribers/internal/handlers"
import mongodb "subscribers/internal/mongodb"

func main() {
	if(len(os.Args) != 2){
		fmt.Println("Usage: " + filepath.Base(os.Args[0]) + " <airport>")
		fmt.Println("Example: " + filepath.Base(os.Args[0]) + " NTE")
		os.Exit(1)
	}
	var topic string = os.Args[1]
	mongodb.Connect()
	mqtt.Connect("sub_db_" + topic)
	time.Sleep(time.Second * 2)
	mqtt.Subscribe(topic, handlers.MessageHandlerDatabase)
	for {} // stays active to receive messages
}
