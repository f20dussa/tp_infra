package handlers

import "fmt"
import "os"
import "time"
import "strings"
import "log"
import "encoding/csv"
import mqtt "github.com/eclipse/paho.mqtt.golang"
import mongodb "subscribers/internal/mongodb"

var MessageHandlerDatabase mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
    fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
	mongodb.InsertData(string(msg.Payload()))
}

var MessageHandlerDatalake mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
	
	msgContent := string(msg.Payload())
	tokens := strings.Split(msgContent, " - ")

	sensorId := tokens[0]
	airportId := tokens[1]
	measureType := tokens[2]
	measureValue := tokens[3]
	timestamp := tokens[4]

	dataLine := []string{sensorId, measureValue, timestamp}
	header := []string{"sensorId", "measureValue", "timestamp"}
	currentTime := time.Now()
	filePath := airportId + "-" + currentTime.Format("02-01-2006") + "-" + measureType + ".csv"

	var csvFile *os.File
	var fileCreated = false
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		csvFile, err = os.Create(filePath)
		if err != nil {
			log.Fatalf("Failed creating file: %s", err)
		}
		fileCreated = true
	} else {
		csvFile, err = os.OpenFile(filePath, os.O_APPEND|os.O_WRONLY, 0755)
		if err != nil {
			log.Fatalf("Failed opening file: %s", err)
		}
	}
	csvwriter := csv.NewWriter(csvFile)
	if fileCreated {
		csvwriter.Write(header)
	}
	csvwriter.Write(dataLine)
	csvwriter.Flush()
	csvFile.Close()
}