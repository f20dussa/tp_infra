package mongodb

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
	bson "go.mongodb.org/mongo-driver/bson"
	mongo "go.mongodb.org/mongo-driver/mongo"
	options "go.mongodb.org/mongo-driver/mongo/options"
)

var database *mongo.Database
var ctx = context.TODO()
var timestamp time.Time

func Connect() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017/")
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	database = client.Database("Infra_Database")
	fmt.Printf("Connexion avec la base de données effectuée !\n")
}

func InsertData(data string) {
	message := strings.Split(data, " - ")
	sensorId := message[0]
	airportId := message[1]
	measureType := message[2]
	measureValue, _ := strconv.Atoi(message[3])
	
	timestamp, err := time.Parse(time.RFC3339, message[4])

	if err != nil {
		log.Fatal(err)
	}

	doc := bson.D{{"sensorId", sensorId}, {"airportId", airportId}, {"measureValue", measureValue}, {"timestamp", timestamp}}

	col := database.Collection(measureType)
	col.InsertOne(ctx, doc)
	fmt.Printf("Insertion de données dans la collection\n")
}
