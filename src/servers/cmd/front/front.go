package main

import (
	"log"
	"net/http"
)

func main() {
	fileServer := http.FileServer(http.Dir("../front/dist"))
	http.Handle("/", http.StripPrefix("/", fileServer))
	err := http.ListenAndServe(":8181", nil)

	if err != nil {
		log.Fatal("Erreur durant le démarrage du serveur : ", err)
		return
	}
}
