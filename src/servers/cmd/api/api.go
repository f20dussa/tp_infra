package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	mongodb "servers/internal/mongodb"
	"time"

	bson "go.mongodb.org/mongo-driver/bson"
	mongo "go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var database *mongo.Database

type Measure struct {
	// ID           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Sensor       string `json:"sensorId,omitempty" bson:"sensorId,omitempty"`
	Airport      string `json:"airportId,omitempty" bson:"airportId,omitempty"`
	MeasureValue int    `json:"measureValue,omitempty" bson:"measureValue,omitempty"`
	// Timestamp    time.Time          `json:"timestamp" bson:"timestamp"`
}

type AverageMeasure struct {
	// ID          primitive.ObjectID  `json:"_id,omitempty" bson:"_id,omitempty"`
	Type string  `json:"type,omitempty" bson:"type"`
	Avg  float64 `json:"avg,omitempty" bson:"avg,omitempty"`
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Bienvenue sur l'API Go !")
	fmt.Println("[API] Page d'accueil atteinte")
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

// Arg 1: Measure
// Arg 2: IATA
// Arg 2: Day/Hour - Start
// Arg 3: Day/Hour - End
func returnMeasure(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[API] Page de retour d'un type de mesure entre 2 dates atteinte")
	w.Header().Set("Content-Type", "application/json")
	enableCors(&w)

	var measures []Measure

	measureType := r.URL.Query()["measure_type"][0]
	startDate := r.URL.Query()["start_date"][0]
	endDate := r.URL.Query()["end_date"][0]
	iata := r.URL.Query()["iata"][0]

	fmt.Println(measureType, startDate, endDate)

	time_start, err := time.Parse(time.RFC3339, startDate)
	if err != nil {
		fmt.Println(err)
	}

	time_end, err := time.Parse(time.RFC3339, endDate)
	fmt.Println(time_start.String(), time_end.String())

	if err != nil {
		fmt.Println(err)
	}

	cur, err := database.Collection(measureType).Find(context.TODO(), bson.M{"airportId": iata, "timestamp": bson.M{
		"$gte": time_start,
		"$lt":  time_end,
	}}, options.Find().SetProjection(bson.M{"timestamp": 0}))

	//cur, err := database.Collection(measureType).Find(context.TODO(), bson.M{""})

	if err != nil {
		mongodb.GetError(err, w)
		return
	}

	for cur.Next(context.TODO()) {
		var measure Measure
		err := cur.Decode(&measure)
		fmt.Println(measure)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(measure)
		measures = append(measures, measure)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(measures)
}

// Arg 1: Day
// Arg 2: iata
func returnAverageMeasures(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[API] Page de retour des moyennes des mesures pour 1 journée atteinte")
	w.Header().Set("Content-Type", "application/json")
	enableCors(&w)

	var averages []AverageMeasure

	date := r.URL.Query()["date"][0] + "T00:00:00Z"
	iata := r.URL.Query()["iata"][0]

	dateTime, _ := time.Parse(time.RFC3339, date)
	dateTimeEnd := dateTime.Add(time.Hour * 24)

	fmt.Println(dateTime, dateTimeEnd, iata)

	mesures := []string{"WIND", "PRES", "TEMP"}

	for _, mesure := range mesures {
		result, err := database.Collection(mesure).Aggregate(context.Background(), bson.A{
			bson.M{"$match": bson.M{"timestamp": bson.M{
				"$gte": dateTime,
				"$lt":  dateTimeEnd,
			},
				"airportId": iata,
			},
			},
			bson.M{"$group": bson.M{
				"_id": nil,
				"avg": bson.M{"$avg": "$measureValue"},
			}}})

		if err != nil {
			mongodb.GetError(err, w)
			return
		}

		for result.Next(context.TODO()) {

			var avgMesure AverageMeasure
			err := result.Decode(&avgMesure)
			fmt.Println(avgMesure)
			if err != nil {
				log.Fatal(err)
			}

			avgMesure.Type = mesure
			averages = append(averages, avgMesure)
		}
	}

	json.NewEncoder(w).Encode(averages)
}

func handleRequests() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/v1/measures/average", returnAverageMeasures)
	http.HandleFunc("/v1/measures", returnMeasure)
	log.Fatal(http.ListenAndServe(":10000", nil))
}

func main() {
	database = mongodb.Connect()
	handleRequests()
}
