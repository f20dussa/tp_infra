echo "Building whole project"

if [[ ! -d build_out ]]
then
   mkdir -p build_out/{publishers,subscribers,servers,front}
fi

cd publishers && go build -o ../build_out/publishers ./... && cd ..
cd subscribers && go build -o ../build_out/subscribers ./... && cd ..
cd servers && go build -o ../build_out/servers ./... && cd ..
cd front && npm install && npm run build && cd ..

echo "Executables written in build_out"
